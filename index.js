const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// [SECTION] MongoDB Connection
// Syntax -> mongoose.connect("SRV LINK", {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connect("mongodb+srv://admin:admin123@zuitt.kbb5wo6.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true, 
	useUnifiedTopology : true
});

// check for connection error...
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));

// if connection works...
db.once("open", () => console.log("We're connected to the cloud database"));

// [SECTION] Mongoose Schemas
// It determines the structure of our documents to be stored in the database
// It acts as our data blueprint and guide

const taskSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Task name is required"]
	},
	status: {
		type: String,
		default: "pending"
	}
})

// [ACTIVITY] Add user schema
const userSchema = new mongoose.Schema({
	username : {
		type: String,
		required: [true, "Username is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	}
})


// [SECTION] Models
const Task = mongoose.model("Task", taskSchema);

// [SECTION] Creation of to-do list routes

app.use(express.json());
// allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

// Business Logic

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		// check if there's already a similar data on the db
		if(result != null && result.name == req.body.name) {
			return res.send("Duplicate found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask)=> {
				if(saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New Task created.");
				}
			})
		}
	})
});


// GET method
app.get("/tasks", (req, res) => {
	// {} inside find() means to get all data
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})


// [ACTIVITY], creating a model
const User = mongoose.model("User", userSchema);
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// [ACTIVITY] POST method
app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username && result.password == req.body.password) {
			return res.send("Duplicate found");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, save)=> {
				if(saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New User created.");
				}
			})
		}
	})
});





app.listen(port, () => console.log(`Server running at port ${port}.`));
